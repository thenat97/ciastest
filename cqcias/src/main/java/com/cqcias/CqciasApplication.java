package com.cqcias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CqciasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CqciasApplication.class, args);
	}

}
