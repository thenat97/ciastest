package com.cqcias.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cqcias.model.Persona;

public interface PersonaDAO extends JpaRepository<Persona, Integer> {

	
	

	List<Persona> findByEstatus(String a);
}
