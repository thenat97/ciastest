package com.cqcias.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cqcias.dao.PersonaDAO;
import com.cqcias.model.Persona;

@RestController
@RequestMapping("personas")
@CrossOrigin
public class PersonaRest {
	
	
	
	
	@Autowired
	private PersonaDAO personaDAO;
	
	@GetMapping("/lista")

	public List<Persona> listar(){
		return personaDAO.findByEstatus("a");
	}
	
	
	

}
