

$.ajax({
    url: 'http://localhost:8080/personas/lista',
    data: [],
    success: function(data){
        let personas = data;
        console.log(personas);
        //Important code starts here to populate table  
        let yourTableHTML = "";
        jQuery.each(personas, function(i,data) {
            $("#tbodyPersonas")
            .append(`<tr   data-toggle="modal" data-target="#modalPersona" 
            data-nombre="${data.nombre}" 
            data-primerApellido="${data.primerApellido}"
            data-segundoApellido="${data.segundoApellido}"
            data-estatus="${data.estatus}"
            data-telefono="${data.telefono}"
            ><td> ${data.nombre}  </td><td> ${data.telefono} </td></tr>`);
        });
        

        $("#tbodyPersonas").on('click','tr',this.data,function (){
            console.log($(this).data());
            let nombre = $(this).data().nombre
            let primerApellido = $(this).data().primerapellido
            let segundoApellido = $(this).data().segundoapellido
            let estatus = $(this).data().estatus
            let telefono = $(this).data().telefono
            $("#nombre").val(nombre)
            $("#primerApellido").val(primerApellido)
            $("#segundoApellido").val(segundoApellido)
            $("#estatus").val(estatus)
            $("#telefono").val(telefono)
        })


    },
    dataType: 'json'
  });

