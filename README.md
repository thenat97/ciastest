# Test CIAS


A continuación se muestra el proyecto de evaluación de CIAS, el proyecto utilizo spring boot para la REST API, 
JQuery para la pagina web y SQL Server 2014 Express para la base de datos.


# Observaciones

- Para la configuración del proyecto solo se tiene que cambiar los datos para la conexión a la base de datos, este archivo se encuentra en springBoot\ciastest\cqcias\src\main\resources\aplication.properties
 - Se agrego un query de SQL para agregar registros a la tabla de personas.
  - Ya que el estatus de las personas es de solo un carácter, la letra "a" significa activo y la letra "i" inactivo.
  - La tabla tiene un estilo de hover, y al dar click en cualquier fila de la tabla, esta abrirá el modal mostrando la información de la persona correspondiente
  - El CORS se dejo abierto, esto con el fin de que sea sencillo el correr la aplicación en otros equipos para su revisión.
   
# Conclusiones

Agradezco que se me haya dado la oportunidad de probar mis habilidades y me gustaría escuchar cualquier clase de feedback que tenga respecto al proyecto, quedo al pendiente de sus comentarios.


