

insert into persona (nombre,primer_apellido,segundo_apellido,telefono,estatus) 
values
('Pepe','Rodriguez','Suarez','1234567891','a'),
('Juan','Torres','Sosa','9876543210','a'),
('Carlos','Barceinas','Aguirre','4567891230','a'),
('Ana','Barcenas','Gutierrez','3692581470','a'),
('Charly','Gonzalez','Rojas','7410852963','a'),
('Abimael','Gomez','Molina','7538694210','a'),
('Joshua','Perez','Castro','1594872630','a'),
('Merida','Diaz','Ortiz','2584697130','a'),
('Clarisa','Martinez','Castro','973128465','a'),
('Eric','Romero','Luna','4826159730','a'),
('Alyssa','Alvarez','Rios','9873216540','i'),
('Mel','Ruiz','Morales','0258147369','i'),
('Lucero','Ramirez','Cabrera','0963741258','i'),
('Abraham','Flores','Godoy','2581436970','i'),
('Roberto','Acosta','Carrizo','1515487963','i'),
('Jose','Herrera','Peralta','2233356489','i')
;

select * from persona;